package fpinscala.errorhandling

import org.scalatest.{MustMatchers, WordSpec}

class EitherTest extends WordSpec with MustMatchers {

  "4.6: map" must {
    "produce an unchanged Left when starting with Left" in {
      val left = Left("error message")
      left.map(a => ???) mustBe left
    }

    "produce a transformed value based on Right.map" in {
      val initial: Either[String, Int] = Right(1)
      initial.map(_.toString) mustBe Right("1")
    }
  }

  "4.6: flatMap" must {
    "produce an unchanged Left when starting with Left" in {
      val left = Left("error message")
      left.flatMap(a => ???) mustBe left
    }

    "produce a transformed value based on Right.map" in {
      val initial: Either[String, Int] = Right(1)
      initial.flatMap(a => Right(a.toString)) mustBe Right("1")
    }

    "produce Left when f 'fails' and produces a Left" in {
      val initial: Either[String, Int] = Right(1)
      initial.flatMap(a => if (a >= 10) Right(a.toString) else Left("1 is less than 10")) mustBe Left("1 is less than 10")
    }
  }

  "4.6: orElse" must {
    "produce an unchanged Right when starting with Right" in {
      val initial: Either[String, Int] = Right(1)
      initial.orElse(Left("foo")) mustBe initial
    }

    "produce the default Either 'b' from a Left" in {
      Left("foo").orElse(Right("bar")) mustBe Right("bar")
    }
  }

  "4.6: map2" must {
    "produce the first Left encountered" in {
      Left("foo").map2(Right("bar"))((a, b) => ???) mustBe Left("foo")
      Right("bar").map2(Left("foo"))((a, b) => ???) mustBe Left("foo")
    }

    "produce the value from 'f' when both Eithers are Right" in {
      Right("foo").map2(Right("bar"))((a, b) => a + b) mustBe Right("foobar")
    }
  }

  "4.7: traverse" must {
    "produce the Left produced by 'f'" in {
      Either.traverse(List("foo", "bar")){ a => Left("error")} mustBe Left("error")
    }

    "produce a Right of transformed values when all calls to 'f' succeed" in {
      val result = Either.traverse(List("foo", "bar", "baz")){ a => Right(a.reverse)}
      result mustBe Right(List("oof", "rab", "zab"))
    }

    "not proceed once 'f' has produced a Left" in {
      Either.traverse(List("foo", "bar", "baz")){ a => a match {
        case "foo" => Left("foo")
        case _ => throw new IllegalStateException("must never get here")
      }} mustBe Left("foo")
    }
  }

  "4.7: sequence" must {
    "produce a Right with an empty List from an empty List" in {
      Either.sequence(Nil) mustBe Right(Nil)
    }

    "produce the first Left encountered in the List" in {
      Either.sequence(List(Right("foo"), Left("bar"), Right("biz"), Left("baz"))) mustBe Left("bar")
    }

    "produce a Right[List[A]] with all values from a List of Rights" in {
      Either.sequence(List(Right("foo"), Right("bar"), Right("biz"), Right("baz"))) mustBe Right(List("foo", "bar", "biz", "baz"))
    }
  }
}
