package fpinscala.laziness

import org.scalatest.{MustMatchers, WordSpec}

class StreamTest extends WordSpec with MustMatchers {

  "5.1:  toList" must {

    "produce an empty List for an Empty Stream" in {
      Empty.toList mustBe List()
    }

    "produce List(1) for a stream with only one value" in {
      Stream(1).toList mustBe List(1)
    }

    "produce List(2, 4, 6, 8) for a Stream with the same values" in {
      Stream(2, 4, 6, 8).toList mustBe List(2, 4, 6, 8)
    }
  }

  "5.2 & 5.13:  take & takeViaUnfold" must {

    "produce an Empty Stream from an Empty Stream" in {
      Empty.take(5) mustBe Empty
      Empty.takeViaUnfold(5) mustBe Empty
    }

    "produce the same Stream when taking the same number of elements as what's in the stream" in {
      val result = List(1, 2, 3, 4, 5)
      Stream(1, 2, 3, 4, 5).take(5).toList mustBe result
      Stream(1, 2, 3, 4, 5).takeViaUnfold(5).toList mustBe result
    }

    "take only the number of elements specified from the head of the Stream" in {
      val result = List(1, 2)
      Stream(1, 2, 3, 4, 5).take(2).toList mustBe result
      Stream(1, 2, 3, 4, 5).takeViaUnfold(2).toList mustBe result
    }

    "takes whatever is available when we ask for more elements than are available" in {
      val result = List(1, 2, 3)
      Stream(1, 2, 3).take(5).toList mustBe result
      Stream(1, 2, 3).takeViaUnfold(5).toList mustBe result
    }

  }

  "5.2:  drop(n)" must {
    "produce an Empty Stream from an Empty Stream" in {
      Empty.drop(2) mustBe Empty
    }

    "produce the same Stream when dropping 0" in {
      Stream(1, 2, 3, 4, 5).drop(0).toList mustBe List(1, 2, 3, 4, 5)
    }

    "drop the correct number of elements from the head of the Stream" in {
      Stream(1, 2, 3, 4, 5).drop(2).toList mustBe List(3, 4, 5)
    }

    "produces Empty when dropping more elements than the Stream contains" in {
      Stream(1, 2, 3, 4, 5).drop(10) mustBe Empty
    }
  }

  "5.3 & 5.5 & 5.13:  takeWhile" must {
    "produce an Empty Stream from an Empty Stream" in {
      Empty.takeWhile(_ => true) mustBe Empty
      Empty.takeWhileViaUnfold(_ => true) mustBe Empty
    }

    "produce an Empty Stream when none of the Stream's elements match the predicate" in {
      val stream = Stream("foo", "bar", "biz", "baz")

      stream.takeWhile(_ == "fizbuz") mustBe Empty
      stream.takeWhileViaUnfold(_ == "fizbuz") mustBe Empty
    }

    "produce a Stream with elements that match the predicate" in {
      val stream = Stream("bar", "baz", "foo", "bad")
      val expected = List("bar", "baz")

      def matcher(str: String) = str.startsWith("ba")

      stream.takeWhile(matcher).toList mustBe expected
      stream.takeWhileViaUnfold(matcher).toList mustBe expected
    }
  }

  "5.4:  forAll" must {
    "be false for an Empty Stream" in {
      Empty.forAll(_ => throw new RuntimeException("will not be called")) mustBe false
    }

    "be false for a Steam in which none of the elements match the predicate" in {
      Stream(1, 3, 5, 7, 9, 11).forAll(_ % 2 == 0) mustBe false
    }

    "be false for a Stream in which any element does not match the predicate" in {
      Stream(2, 4, 6, 7, 8, 10).forAll(_ % 2 == 0) mustBe false
    }

    "be true for a Stream in which all elements match the predicate" in {
      Stream(2, 4, 6, 8, 10).forAll(_ % 2 == 0) mustBe true
    }
  }

  "5.7 & 5.13:  map & mapViaUnfold" must {
    "produce Empty from Empty" in {
      Empty.map(_ => throw new RuntimeException("must never be called")) mustBe Empty
      Empty.mapViaUnfold(_ => throw new RuntimeException("must never be called")) mustBe Empty
    }

    "produce a Stream populated by values transformed by f" in {
      Stream(2, 3, 4, 5).map(x => x * 2).toList mustBe List(4, 6, 8, 10)
      Stream(2, 3, 4, 5).mapViaUnfold(x => x * 2).toList mustBe List(4, 6, 8, 10)
    }
  }

  "5.7:  filter" must {
    "produce Empty from Empty" in {
      Empty.filter(_ => throw new RuntimeException("must never be called")) mustBe Empty
    }

    "produce Empty when no elements match the predicate" in {
      Stream("foo", "bar", "biz", "baz").filter(_ == "fizbuz") mustBe Empty
    }

    "produce a Stream with elements matching the predicate" in {
      Stream("bar", "foo", "biz", "baz").filter(_.startsWith("ba")).toList mustBe List("bar", "baz")
    }
  }

  "5.7:  append" must {
    "produce Empty when appending Empty to Empty" in {
      Empty.append(Empty) mustBe Empty
    }

    "produce the original Stream when appending Empty to it" in {
      Stream(1, 2, 3, 4, 5).append(Empty).toList mustBe List(1, 2, 3, 4, 5)
    }

    "produce the appended Stream when it is appended to Empty" in {
      Empty.append(Stream(1, 2, 3, 4, 5)).toList mustBe List(1, 2, 3, 4, 5)
    }

    "produce the a Stream with the conents of s appended to the original" in {
      Stream(1, 2, 3).append(Stream(4, 5, 6)).toList mustBe List(1, 2, 3, 4, 5, 6)
    }
  }

  def stringToStream(from: String): Stream[Char] = {
    def go(src: String, acc: Stream[Char]): Stream[Char] = {
      if (src.isEmpty)
        acc
      else {
        val t = src.tail
        go(t, Cons(() => src.head, () => acc))
      }
    }

    go(from.reverse, Empty)
  }

  "5.7 stringToStream" must {
    "produce Empty from an empty String" in {
      stringToStream("") mustBe Empty
    }

    "produce a Stream of the String's component characters" in {
      stringToStream("foobar").toList mustBe List('f', 'o', 'o', 'b', 'a', 'r')
    }
  }

  "5.7:  flatMap" must {
    "produce Empty from Empty" in {
      Empty.flatMap(_ => throw new RuntimeException("this should not be called")) mustBe Empty
    }

    "produce a Stream of based on the conversions of f" in {
      Stream("foo", "bar").flatMap(stringToStream).toList mustBe List('f', 'o', 'o', 'b', 'a', 'r')
    }
  }

  import Stream._

  "5.8 & 5.12:  ones & onesViaUnfold" must {
    "produce lots of 1's" in {
      val result = List(1, 1, 1, 1, 1, 1, 1, 1, 1, 1)
      ones.take(10).toList mustBe result
    }
  }

  "5.8 & 5.12: constant & constantViaUnfold" must {
    "produce a stream of 2's" in {
      val result = List(2, 2, 2, 2, 2)
      constant(2).take(5).toList mustBe result
      constantViaUnfold(2).take(5).toList mustBe result
    }

    "produces a stream of foo's" in {
      val result = List("foo", "foo", "foo", "foo", "foo")
      constant("foo").take(5).toList mustBe result
      constantViaUnfold("foo").take(5).toList mustBe result
    }
  }

  "5.9 & 5.12:  from & fromViaUnfold" must {
    "produce a stream that counts upwards from 1" in {
      val result = List(1, 2, 3, 4, 5)
      from(1).take(5).toList mustBe result
      fromViaUnfold(1).take(5).toList mustBe result
    }

    "produce a stream that counts upwards from 100" in {
      val result = List(100, 101, 102, 103, 104)
      from(100).take(5).toList mustBe result
      fromViaUnfold(100).take(5).toList mustBe result
    }
  }

  "5.10 & 5.12:  fibStream & fibStreamViaUnfold" must {
    "produce a stream of Fibonacci numbers" in {
      val listOfFibs = List(0, 1, 1, 2, 3, 5, 8)
      fibStream().take(7).toList mustBe listOfFibs
      fibStreamViaUnfold().take(7).toList mustBe listOfFibs
    }
  }

  "5.11:  unfold" must {
    "produce Empty when f produces None" in {
      unfold("start") { _ => None } mustBe Empty
    }

    "produce a Stream from 1 to 5 and then terminate" in {
      unfold(1) { n => if (n > 5) None else Some((n, n + 1)) }.toList mustBe List(1, 2, 3, 4, 5)
    }
  }

  "5.13: zipWithViaUnfold" must {
    def add(a: Int, b: Int) = a + b

    "produce Empty if either input is Empty" in {
      val doNotCall = (a: Int, b: Int) => throw new IllegalStateException()

      val noInts: Stream[Int] = Empty
      val someInts = Stream(2, 4, 6, 8)
      val empty = List()

      zipWithViaUnfold(noInts, noInts, doNotCall).toList mustBe empty
      noInts.zipWith(noInts)(doNotCall).toList mustBe empty

      zipWithViaUnfold(noInts, someInts, doNotCall).toList mustBe empty
      noInts.zipWith(someInts)(doNotCall).toList mustBe empty

      zipWithViaUnfold(someInts, noInts, doNotCall).toList mustBe empty
      someInts.zipWith(noInts)(doNotCall).toList mustBe empty
    }

    "produce a stream of summed values for two Stream[Int] and a summing function" in {
      val expected = List(5, 7, 9)

      zipWithViaUnfold(Stream(1, 2, 3), Stream(4, 5, 6), add).toList mustBe expected
      Stream(1, 2, 3).zipWith(Stream(4, 5, 6))(add).toList mustBe expected
    }

    "produce a stream of summed values for the pairs available when the Streams are unbalanced" in {
      val expected = List(5, 7, 9)
      zipWithViaUnfold(Stream(1, 2, 3, 10, 11, 12), Stream(4, 5, 6), add).toList mustBe expected
      Stream(1, 2, 3, 10, 11, 12).zipWith(Stream(4, 5, 6))(add).toList mustBe expected

      zipWithViaUnfold(Stream(1, 2, 3), Stream(4, 5, 6, 10, 11, 12), add).toList mustBe expected
      Stream(1, 2, 3).zipWith(Stream(4, 5, 6, 10, 11, 12))(add).toList mustBe expected
    }

    "produce the product of the character count and the Int for a Stream[String] and List[Int]" in {
      val multiplyByCharCount = (a: String, b: Int) => a.length * b
      val expected = List(27, 14, 15)

      zipWithViaUnfold(Stream("foo", "fizbuzz", "Scala"), Stream(9, 2, 3), multiplyByCharCount).toList mustBe expected
      Stream("foo", "fizbuzz", "Scala").zipWith(Stream(9, 2, 3))(multiplyByCharCount).toList mustBe expected
    }
  }

  "5.13:  zipAllViaUnfold, though not the correct implementation" must {

    def addOrDefaultToAvailable(a: Option[Int], b: Option[Int])= (a, b) match {
      case (a: Some[Int], b: Some[Int]) => a.get + b.get
      case _ => a.getOrElse(b.get)
    }

    "produces a Stream of summed digits when both Streams have the same number of elements" in {
      val expected = List(5, 7, 9)
      zipAllViaUnfold(Stream(1, 2, 3), Stream(4, 5, 6), addOrDefaultToAvailable).toList mustBe expected
    }

    "produces a default value when the first Stream's elements are exhausted before of the second" in {
      val expected = List(5, 7, 9, 10, 11, 12)
      zipAllViaUnfold(Stream(1, 2, 3), Stream(4, 5, 6, 10, 11, 12), addOrDefaultToAvailable).toList mustBe expected
    }

    "produces a default value when the second Stream's elements are exhausted before the first" in {
      val expected = List(5, 7, 9, 10, 11, 12)
      zipAllViaUnfold(Stream(1, 2, 3, 10, 11, 12), Stream(4, 5, 6), addOrDefaultToAvailable).toList mustBe expected
    }
  }

  "5.13:  zipAll" must {

    "produce a Stream of pairs of Some[T] where both streams are of equal length" in {
      val expected = List((Some(1), Some(4)), (Some(2), Some(5)), (Some(3), Some(6)))
      Stream(1, 2, 3).zipAll(Stream(4, 5, 6)).toList mustBe expected
    }

    "produce a Stream with Nones in the first position of the tuple when the 2nd Stream is longer" in {
      val expected = List((Some(1), Some(4)), (Some(2), Some(5)), (Some(3), Some(6)),
                          (None, Some(10)), (None, Some(11)), (None, Some(12)))
      Stream(1, 2, 3).zipAll(Stream(4, 5, 6, 10, 11, 12)).toList mustBe expected
    }

    "produce a Stream with Nones in the second position of the tuple when the 1st Stream is longer" in {
      val expected = List((Some(1), Some(4)), (Some(2), Some(5)), (Some(3), Some(6)),
        (Some(10), None), (Some(11), None), (Some(12), None))
      Stream(1, 2, 3, 10, 11, 12).zipAll(Stream(4, 5, 6)).toList mustBe expected
    }
  }

  "5.14:  startsWith" must {

    "produce `false` when one Stream is not the prefix of another" in {
      Stream(1, 2, 3).startsWith(Stream(4, 5, 6)) mustBe false
    }

    "produce `true` when one Stream is the prefix of another" in {
      Stream(1, 2, 3, 4, 5, 6).startsWith(Stream(1, 2, 3)) mustBe true
    }

    "work for very long streams when one Stream is not the prefix of the other" in {
      val first = constant(5).take(5000)
      val second = constant(5).take(4999).append(Stream(1))
      first.startsWith(second) mustBe false
    }

    "work for very long streams when one Stream is the prefix of the other" in {
      val first = constant(5).take(5000)
      val second = constant(5).take(5000)
      first.startsWith(second) mustBe true
    }

    "produce `false` when either Stream is Empty" in { // I'm really guessing about these
      Empty.startsWith(Stream(4, 5, 6)) mustBe false
      Stream(4, 5, 6).startsWith(Empty) mustBe false
      Empty.startsWith(Empty) mustBe false
    }
  }

  "5.15:  Stream(1, 2, 3).tails"  must {
    val tailsList = Stream(1, 2, 3).tails.toList

    "produce Stream(1, 2, 3) at index 0" in {
      tailsList(0).toList mustBe List(1, 2, 3)
    }

    "produce Stream(2, 3) at index 1" in {
      tailsList(1).toList mustBe List(2, 3)
    }

    "produce Stream(3) at index 2" in {
      tailsList(2).toList mustBe List(3)
    }

    "produce Empty at index 3" in {
      tailsList(3).toList mustBe List()
    }
  }
}
