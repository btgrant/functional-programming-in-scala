import fpinscala.laziness._

val closedStreamOfInts = Stream(2, 4, 6, 8)

closedStreamOfInts.toList