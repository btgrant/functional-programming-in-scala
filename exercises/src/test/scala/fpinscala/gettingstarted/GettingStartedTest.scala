package fpinscala.gettingstarted

import MyModule.fib
import PolymorphicFunctions.isSorted

class GettingStartedTest extends org.scalatest.FunSuite {

  test("2.1:  Fibonacci sequence") {
    val fibs = Map(0 -> 0, 1 -> 1, 2 -> 1, 3 -> 2, 4 -> 3, 5 -> 5, 12 -> 144)

    for ((input, result) <- fibs) {
      assert(fib(input) === result)
    }
  }

  test("2.2:  An array of numbers in natural order is sorted") {
    val original = Array(1, 2, 3, 4, 5)
    assert(isSorted(original, (x: Int, y: Int) => x < y))
  }

  test("2.2:  An array of numbers in out of natural ordering is not sorted") {
    val original = Array(1, 2, 7, 4, 5)
    assert(!isSorted(original, (x: Int, y: Int) => x < y))
  }

  test("2.2:  An array of characters in alphabetical order is sorted") {
    val original = Array('a', 'b', 'c', 'd', 'e', 'f')
    assert(isSorted(original, (x: Char, y: Char) => y.compare(x) > 0))
  }

  test("2.2:  An array of characters out of alphabetical ordering is not sorted") {
    val original = Array('a', 'b', 'c', 'x', 'e', 'f')
    assert(!isSorted(original, (x: Char, y: Char) => y.compare(x) > 0))
  }
}
