package fpinscala.datastructures

import org.scalatest.{MustMatchers, WordSpec}

class TreeTest extends WordSpec with MustMatchers {

  "3.25, 3.29:  size & sizeByFold" must {
    "produce 1 for a Leaf" in {
      Tree.size(Leaf("foo")) mustBe 1
      Tree.sizeByFold(Leaf("foo")) mustBe 1
    }

    "produce 3 for a Branch with two Leaf nodes" in {
      Tree.size(Branch(Leaf("foo"), Leaf("bar"))) mustBe 3
      Tree.sizeByFold(Branch(Leaf("foo"), Leaf("bar"))) mustBe 3
    }

    "produce 5 for a two Branches and three Leaf nodes" in {
      Tree.size(Branch(Leaf("foo"), Branch(Leaf("bar"), Leaf("baz")))) mustBe 5
      Tree.sizeByFold(Branch(Leaf("foo"), Branch(Leaf("bar"), Leaf("baz")))) mustBe 5
    }
  }

  "3.26, 3.29:  maximum and maxiumuByFold" must {
    "produce the value supplied by a Leaf" in {
      Tree.maximum(Leaf(5)) mustBe 5
      Tree.maximum(Leaf(3)) mustBe 3
      Tree.maximum(Leaf(0)) mustBe 0

      Tree.maximumByFold(Leaf(5)) mustBe 5
      Tree.maximumByFold(Leaf(3)) mustBe 3
      Tree.maximumByFold(Leaf(0)) mustBe 0
    }

    "produce the maxiumum value found in a Tree" in {
      val tree = Branch(Branch(Leaf(5), Leaf(12)), Branch(Leaf(42), Leaf(-5)))
      Tree.maximum(tree) mustBe 42

      Tree.maximumByFold(tree) mustBe 42
    }
  }

  "3.27 & 3.29:  depth & depthByFold" must {
    "produce 1 for a Leaf" in {
      val input = Leaf(5)
      Tree.depth(input) mustBe 1

      Tree.depthByFold(input) mustBe 1
    }

    "produce 2 for a Branch with two Leaves" in {
      val input = Branch(Leaf(5), Leaf(2))
      Tree.depth(input) mustBe 2

      Tree.depthByFold(input) mustBe 2
    }

    "produce 5 for a Tree with the right Branch 5 levels deep" in {
      val input = Branch(
        Leaf(2), Branch(
          Leaf(3), Branch(
            Leaf(4), Branch(
              Leaf(5), Leaf(5)))))

      Tree.depth(input) mustBe 5

      Tree.depthByFold(input) mustBe 5
    }

    "produce 5 for a Tree with the left Branch 5 levels deep" in {
      val input = Branch(Branch(Branch(Branch(Leaf(5), Leaf(5)), Leaf(4)), Leaf(3)), Leaf(2))
      Tree.depth(input) mustBe 5

      Tree.depthByFold(input) mustBe 5
    }
  }

  "3.28, 3.29: map & mapByFold" must {
    "produce the original Tree when f makes no changes" in {
      val original = Branch(Leaf("foo"), Leaf("bar"))
      Tree.map[String,String](original, (t) => t) mustBe original

      Tree.mapByFold[String,String](original, (t) => t) mustBe original
    }

    "produce a Tree with the original value correctly mapped to new values" in {
      var original = Branch(Leaf(1), Branch(Leaf(2), Branch(Leaf(3), Leaf(4))))
      var transformed = Branch(Leaf("foo"), Branch(Leaf("bar"), Branch(Leaf("biz"), Leaf("baz"))))

      def transform(v: Int): String = v match {
        case 1 => "foo"
        case 2 => "bar"
        case 3 => "biz"
        case 4 => "baz"
      }

      Tree.map[Int,String](original, transform) mustBe transformed

      Tree.mapByFold[Int,String](original, transform) mustBe transformed

      original = Branch(Branch(Leaf(2), Branch(Leaf(3), Leaf(4))), Leaf(1))
      transformed = Branch(Branch(Leaf("bar"), Branch(Leaf("biz"), Leaf("baz"))), Leaf("foo"))

      Tree.map[Int,String](original, transform) mustBe transformed

      Tree.mapByFold[Int,String](original, transform) mustBe transformed
     }
  }


}
