
import fpinscala.datastructures.List._
import fpinscala.datastructures.{List, Nil}
import org.scalatest.FunSuite

class ListTest extends FunSuite {

  test("3.1:  x is 3") {
    assert(3 === List.x)
  }

  test("3.2 & 3.4:  Tail of a List of three elements is the last two elements") {
    assert(List(2, 3) === tail(List(1, 2, 3)))
    assert(List(2, 3) === tailByDrop(List(1, 2, 3)))
  }

  test("3.2 & 3.4:  Tail of a List of two elements is the last element") {
    assert(List(2) === tailByDrop(List(1, 2)))
  }

  test("3.2 & 3.4:  Tail of Nil is Nil") {
    assert(Nil === tail(Nil))
    assert(Nil === tailByDrop(Nil))
  }

  test("3.3:  setHead") {
    assert(List(5, 2, 3) === setHead(List(1, 2, 3), 5))
  }

  test("3.3:  setHead on Nil") {
    assert(List(5) === setHead(Nil, 5))
  }

  test("3.4:  drop 2 on a List with 4 elements") {
    assert(List(3, 4) === drop(List(1, 2, 3, 4), 2))
  }

  test("3.5:  dropWhile without any matches produces the original List") {
    assert(List(1, 2, 3) === dropWhile(List(1, 2, 3), (_: Int) => false))
  }

  test("3.5:  dropWhile with Nil results in Nil") {
    assert(Nil === dropWhile(Nil, (_: Any) => false))
  }

  test("3.5:  dropWhile") {
    assert(List('b', 'c', 'd') === dropWhile(List('A', 'b', 'c', 'd', 'E'), (x: Char) => x
      .isUpper))
  }

  test("3.6:  init with Nil produces Nil") {
    assert(Nil === init(Nil))
  }

  test("3.6:  init with a List of 4 elements drops the last") {
    assert(List(1, 2, 3) === init(List(1, 2, 3, 4)))
  }

  test("3.9 & 3.11:  Compute the length of a List of 5 elements with foldRight") {
    assert(5 === length(List(1, 2, 3, 4, 5)))
    assert(5 === lengthWithFoldLeft(List(1, 2, 3, 4, 5)))
  }

  test("3.9 & 3.11:  Compute the length of Nil with foldRight") {
    assert(0 === length(Nil))
    assert(0 === lengthWithFoldLeft(Nil))
  }

  def foldLeftSum(l: List[Int]): Int = {
    foldLeft(l, 0)(_ + _)
  }

  test("3.11:  Summation with foldLeft") {
    assert(24 === sumByFoldLeft(List(10, 5, 2, 3, 3, 1)))
  }

  test("3.11:  Summation of Nil with foldLeft is 0") {
    assert(0 === sumByFoldLeft(Nil))
  }

  /*test("productByFoldLeft for Nil is 0") { // TODO why not?
    assert(0 === productByFoldLeft(Nil))
  }*/

  test("3.11:  productByFoldLeft for List with a 0 is 0") {
    assert(0 === productByFoldLeft(List(2, 4, 6, 7, 0, 1, 4)))
  }

  test("3.11:  productByFoldLeft for a list of numbers") {
    assert(60 === productByFoldLeft(List(2, 1, 5, 6)))
  }

  test("3.12:  Reversing Nil is Nil") {
    assert(Nil === reverse(Nil))
  }

  test("3.12:  Reversing list of 1, 2, 3") {
    val reversed = reverse(List(1, 2, 3))
    assert(List(3, 2, 1) === reversed)
  }

  test("3.14:  Appending Nil to Nil is Nil") {
    assert(Nil === append(Nil, Nil))
    assert(Nil === appendInTermsOfFold(Nil, Nil))
  }

  test("3.14:  Appending a List to Nil is the List") {
    assert(List(1, 2, 3) === append(List(1, 2, 3), Nil))
    assert(List(1, 2, 3) === appendInTermsOfFold(List(1, 2, 3), Nil))
  }

  test("3.14:  Appending one list to another") {
    assert(List(1, 2, 3, 10, 11, 12) === append(List(1, 2, 3), List(10, 11, 12)))
    assert(List(1, 2, 3, 10, 11, 12) === appendInTermsOfFold(List(1, 2, 3), List(10, 11, 12)))
  }

  test("3.16:  addOne with Nil") {
    assert(Nil === addOne(Nil))
  }

  test("3.16:  addOne with List(1, 2, 3)") {
    assert(List(2, 3, 4) === addOne(List(1, 2, 3)))
  }

  test("3.17:  turn list of Doubles into Strings with Nil") {
    assert(Nil === doublesToStrings(Nil))
  }

  test("3.17:  turn list of Doubles into Strings with a list of Doubles") {
    assert(List("20.5", "13.2", "12.9") === doublesToStrings(List(20.5, 13.2, 12.9)))
  }

  test("3.18:  map +1 with Nil") {
    assert(Nil === map(List[Int]())(_ + 1))
  }

  test("3.18:  map +1 with List(1, 2, 3)") {
    assert(Nil === map(List[Int]())(_ + 1))
    assert(List(2, 3, 4) ===  map(List(1, 2, 3))(_ + 1))
  }

  test("3.19 & 3.21:  filtering an empty List produces Nil") {
    assert(Nil === filter(List())(_ => throw new IllegalStateException("must not be called")))
    assert(Nil === filterByFlatMap(List())(_ => throw new IllegalStateException("must not be called")))
  }

  test("3.19 & 3.21:  filtering all elements of a List produces Nil") {
    assert(Nil === filter(List(1, 2, 3, 4)){_ => false})
    assert(Nil === filterByFlatMap(List(1, 2, 3, 4)){_ => false})
  }

  test("3.19 & 3.21:  filtering a List produces a List with elements matching the predicate") {
    assert(List(2, 4, 6) === filter(List(1, 2, 3, 4, 5, 6, 7)){_ % 2 == 0})
    assert(List(2, 4, 6) === filterByFlatMap(List(1, 2, 3, 4, 5, 6, 7)){_ % 2 == 0})
  }

  test("3.20:  flatMap on Nil produces Nil") {
    assert(Nil === flatMap(Nil) { _ => throw new IllegalStateException("must not be called")})
  }

  test("3.20:  flatMap(List(1,2,3))(i => List(i,i)) should result in List(1,1,2,2,3,3)") {
    assert(List(1,1,2,2,3,3) === flatMap(List(1,2,3))(i => List(i,i)))
  }

  test("3.22:  addPairwise produces Nil if either input is Nil") {
    assert(Nil === addPairwise(Nil, Nil))
    assert(Nil === addPairwise(Nil, List(1, 2, 3)))
    assert(Nil === addPairwise(List(1, 2, 3), Nil))
  }

  test("3.22:  addPairwise for two List[Int] sums the elements at the same indices") {
    assert(List(5,7,9) === addPairwise(List(1,2,3), List(4,5,6)))
  }

  test("3.22:  addPairwise works for unbalanced Lists") {
    assert(List(5,7,9) === addPairwise(List(1,2,3,10,11,12), List(4,5,6)))
    assert(List(5,7,9) === addPairwise(List(1,2,3), List(4,5,6,10,11,12)))
  }

  test("3.23:  zipWith produces Nil if either input is Nil") {
    assert(Nil === zipWith[Int, Int, Int](Nil, Nil, (_, _) => ???))
    assert(Nil === zipWith[Int, Int, Int](Nil, List(1, 2, 3), (_, _) => ???))
    assert(Nil === zipWith[Int, Int, Int](List(1, 2, 3), Nil, (_, _) => ???))
  }

  test("3.23:  zipWith for two List[Int] and a summing function sums the elements at the same indices") {
    assert(List(5,7,9) === zipWith(List(1,2,3), List(4,5,6), (a: Int, b: Int) => a + b))
  }

  test("3.23:  zipWith works for unbalanced Lists") {
    assert(List(5,7,9) === zipWith(List(1,2,3,10,11,12), List(4,5,6), (a: Int, b: Int) => a + b))
    assert(List(5,7,9) === zipWith(List(1,2,3), List(4,5,6,10,11,12), (a: Int, b: Int) => a + b))
  }

  test("3.23:  zipWith for List[String], List[Int] and a function with multiplies the String length by the Int") {
    assert(List(27, 14, 15) === zipWith(List("foo", "fizbuzz", "Scala"), List(9, 2, 3), (a: String, b: Int) => a.length * b))
  }

  test("3.24:  Nil is not a subsequence of a populated List") {
    assert(!hasSubsequence(List(1, 2, 3), Nil))
  }

  test("3.24:  Nothing is a subsequence of Nil") {
    assert(!hasSubsequence(Nil, List(1, 2, 3)))
    assert(!hasSubsequence(Nil, Nil))
  }

  test("3.24:  Book examples for subsequence") {
    assert(hasSubsequence(List(1,2,3,4), List(1,2,3)))
    assert(hasSubsequence(List(1,2,3,4), List(1,2)))
    assert(hasSubsequence(List(1,2,3,4), List(4)))
  }

  ignore("3.24:  Not subsequences") { // TODO complete
    assert(!hasSubsequence(List(1,2,3,4), List(1,3)))
  }
}
