package fpinscala.laziness

import scala.annotation.tailrec

sealed trait Stream[+A] {
  import Stream.unfold

  def foldRight[B](z: => B)(f: (A, => B) => B): B = // The arrow `=>` in front of the argument type `B` means that the function `f` takes its second argument by name and may choose not to evaluate it.
    this match {
      case Cons(h,t) => f(h(), t().foldRight(z)(f)) // If `f` doesn't evaluate its second argument, the recursion never occurs.
      case _ => z
    }

  def exists(p: A => Boolean): Boolean =
    foldRight(false)((a, b) => p(a) || b) // Here `b` is the unevaluated recursive step that folds the tail of the stream. If `p(a)` returns `true`, `b` will never be evaluated and the computation terminates early.

  @tailrec
  final def find(f: A => Boolean): Option[A] = this match {
    case Empty => None
    case Cons(h, t) => if (f(h())) Some(h()) else t().find(f)
  }

  /* Exercise 5.1
   * Write a function to convert a Stream to a List, which will force its evaluation and let you
   * look at it in the REPL. You can convert to the regular List type in the standard library.
   */
  def toList: List[A] = { // TODO refactor to a fold?
    @tailrec
    def go(src: Stream[A], acc: List[A]): List[A] = {
      src match {
        case Empty => acc
        case Cons(x, xs) => go(xs(), acc :+ x())
      }
    }

    go(this, List())
  }

  /* Exercise 5.1
   * Write the function take(n) for returning the first n elements of a Stream, and drop(n) for
   * skipping the first n elements of a Stream.
   */
  def take(n: Int): Stream[A] = {
    /* results were in reverse order
    def go(src: Stream[A], acc: Stream[A], count: Int): Stream[A] = {
      if (count == 0)
        acc
      else src match {
        case Empty => acc
        case Cons(x, xs) => go(xs(), Cons(x, () => acc), count - 1)
      }
    }
    go(this, Empty, n) */

    if (n == 0)
      Empty
    else this match {
      case Cons(h, t) => Cons(h, () => t().take(n - 1))
      case _ => Empty
    }
  }

  /* Exercise 5.13
   * Use unfold to implement take. */
  def takeViaUnfold(n: Int): Stream[A] = {
    unfold((0, this)) { countAndStream =>
      if (countAndStream._1 == n) {
        None
      } else {
        countAndStream._2 match {
          case Cons(h, t) => Some((h(), (countAndStream._1 + 1, t())))
          case _ => None
        }
      }
    }
  }

  def drop(n: Int): Stream[A] = {
    @tailrec
    def go(src: Stream[A], count: Int): Stream[A] = {
      if (count == n)
        src
      else src match {
        case Empty => src
        case Cons(_, xs) => go(xs(), count + 1)
      }
    }

    go(this, 0)
  }

  /**
    * Write the function takeWhile for returning all starting elements of a Stream that
    * match the given predicate.
    */
  def takeWhile(p: A => Boolean): Stream[A] = {
    /* Exercise 5.3
    this match {
      case Empty => Empty
      case Cons(h, t) => if (p(h())) Cons(h, () => t().takeWhile(p)) else Empty
    }*/

    /* Exercise 5.5:  Use foldRight to implement takeWhile. */
    foldRight[Stream[A]](Empty) { (srcEl, acc) => if (p(srcEl)) Cons(() => srcEl, () => acc) else Empty }
  }

  /* Exercise 5.13
   * Use unfold to implement takeWhile. */
  def takeWhileViaUnfold(p: A => Boolean): Stream[A] = {
    unfold(this) {
      case Cons(h, t) =>
        val head = h()
        if (p(head))
          Some(head, t())
        else
          None
      case _ => None
    }
  }

  /* Exercise 5.4
   * Implement forAll, which checks that all elements in the Stream match a given predicate.
   * Your implementation should terminate the traversal as soon as it encounters a non-matching
   * value. */
  def forAll(p: A => Boolean): Boolean = {
    @tailrec
    def go(src: Stream[A]): Boolean = src match {
      case Empty => true
      case Cons(h, t) => if (!p(h())) false else go(t())
    }

    if (this == Empty)
      false
    else
      go(this)
  }

  /* TODO Exercise 5.6:  Hard: Implement headOption using foldRight. */
  def headOption: Option[A] = ???

  /* Exercise 5.7
   * Implement map using foldRight.
   * Part of the exercise is writing your own function signatures. */
  def map[B](f: A => B): Stream[B] = {
    foldRight(Stream[B]())((srcEl, acc) => Cons(() => f(srcEl), () => acc))
  }


  /* Exercise 5.13
   * Use unfold to implement map. */
  def mapViaUnfold[B](f: A => B): Stream[B] = {
    unfold(this) {
      case Cons(h, t) => Some((f(h()), t()))
      case _ => None
    }
  }

  /* Exercise 5.7
   * Implement filter using foldRight.
   * Part of the exercise is writing your own function signatures. */
  def filter(p: A => Boolean): Stream[A] =
    foldRight(Stream[A]()) { (srcEl, acc) => if (p(srcEl)) Cons(() => srcEl, () => acc) else acc }

  /* Exercise 5.7
   * Implement append using foldRight.
   * The append method should be non-strict in its argument.
   * Part of the exercise is writing your own function signatures. */
  def append[B >: A](x: => Stream[B]): Stream[B] =
    foldRight(x) { (srcEl, acc) => Cons(() => srcEl, () => acc) }

  /* Exercise 5.7
   * Implement flatMap using foldRight.
   * Part of the exercise is writing your own function signatures. */
  def flatMap[B](f: A => Stream[B]): Stream[B] = {
    foldRight(Stream[B]()) { (srcEl, acc) => f(srcEl).append(acc) }
  }

  /* Exercise 5.13
   * Use unfold to implement zipAll. The zipAll function should continue the traversal as long as
   * either stream has more elements—it uses Option to indicate whether each stream has been
   * exhausted. */
  def zipAll[B](s2: Stream[B]): Stream[(Option[A], Option[B])] =
    unfold((this, s2)) {
      case (Cons(aH, aT), Cons(bH, bT)) => Some(((Some(aH()), Some(bH())), (aT(), bT())))
      case (Empty, Cons(bH, bT)) => Some(((None, Some(bH())), (Empty, bT())))
      case (Cons(aH, aT), Empty) => Some(((Some(aH()), None), (aT(), Empty)))
      case _ => None
    }


  def zipWith[B,C](s2: Stream[B])(f: (A,B) => C): Stream[C] =
    unfold((this, s2)) {
      case (Cons(aH, aT), Cons(bH, bT)) => Some((f(aH(), bH()), (aT(), bT())))
      case _ => None
    }

   /* Exercise 5.14
    * Implement startsWith using functions you’ve written. It should check if one Stream is a prefix
    * of another. For instance, Stream(1,2,3) startsWith Stream(1,2) would be true. */
  def startsWith[B](s: Stream[B]): Boolean =
    zipWith(s)((a: A, b: B) => a == b).forAll(_ == true)

  /* Exercise 5.15
   * Implement tails using unfold. For a given Stream, tails returns the Stream of suf- fixes of the
   * input sequence, starting with the original Stream. For example, given Stream(1,2,3), it would
   * return Stream(Stream(1,2,3), Stream(2,3), Stream(3), Stream()). */
  def tails: Stream[Stream[A]] = {
    unfold((Some(this).asInstanceOf[Option[Stream[A]]], false)) {
      case (src: Some[Stream[A]], false) => Some( (src.get, (Some(src.get), true)))
      case (Some(Cons(h, t)), _)         => Some( (t(), (Some(t()), true) ))
      case (Some(Empty), _)              => Some( (Empty, (None, true)) )
      case (None, _)                     => None
    }
  }
}

case object Empty extends Stream[Nothing]
case class Cons[+A](h: () => A, t: () => Stream[A]) extends Stream[A]

object Stream {
  def cons[A](hd: => A, tl: => Stream[A]): Stream[A] = {
    lazy val head = hd
    lazy val tail = tl
    Cons(() => head, () => tail)
  }

  def empty[A]: Stream[A] = Empty

  def apply[A](as: A*): Stream[A] =
    if (as.isEmpty) empty
    else cons(as.head, apply(as.tail: _*))

  val ones: Stream[Int] = constant(1)

  /* Exercise 5.8
   * Generalize ones slightly to the function constant, which returns an infinite Stream of a given value. */
  def constant[A](a: A): Stream[A] = Stream.cons(a, constant(a))

  /* Exercise 5.9
   * Write a function that generates an infinite stream of integers, starting from n, then n + 1,
   * n + 2, and so on. */
  def from(n: Int): Stream[Int] = cons(n, from(n + 1))

  /* Exercise 5.10
   * Write a function fibs that generates the infinite stream of
   * Fibonacci numbers: 0, 1, 1, 2, 3, 5, 8, and so on. */
  def fibStream(): Stream[Int] = {
    def go(prev1: Int, prev2: Int): Stream[Int] = {
      cons(prev1, go(prev2, prev1 + prev2))
    }

    go(0, 1)
  }

  /* Exercise 5.11
   * Write a more general stream-building function called unfold. It takes an initial state, and a
   * function for producing both the next state and the next value in the generated stream. */
  def unfold[A, S](initialState: S)(nextValueAndState: S => Option[(A, S)]): Stream[A] = {
    nextValueAndState(initialState) match {
      case None => Empty
      case Some((a, b)) => cons(a, unfold(b)(nextValueAndState))
    }
  }

  /* Exercise 5.12
   * Write fibs in terms of unfold. */
  def fibStreamViaUnfold(): Stream[Int] =
    unfold((0, 1)) { lastTwo => Some((lastTwo._1, (lastTwo._2, lastTwo._1 + lastTwo._2))) }

  /* Exercise 5.12
   * Write from in terms of unfold. */
  def fromViaUnfold(n: Int): Stream[Int] =
    unfold(n) { seed => Some((seed, seed + 1)) }

  /* Exercise 5.12
   * Write constant in terms of unfold. */
  def constantViaUnfold[A](a: A): Stream[A] = unfold(a) { constant => Some((constant, constant)) }

  /* Exercise 5.12
   * Write ones in terms of unfold. */
  val onesViaUnfold: Stream[Int] = constantViaUnfold(1)

  /* Exercise 5.13
   * Use unfold to implement zipWith as in chapter 3
   */
  def zipWithViaUnfold[A, B, C](a: Stream[A], b: Stream[B], f: (A, B) => C): Stream[C] = unfold((a, b)) {
    case (Cons(aH, aT), Cons(bH, bT)) => Some((f(aH(), bH()), (aT(), bT())))
    case _ => None
  }

  /* Exercise 5.13
   * I implemented this without reading the signature for zipAll */
  def zipAllViaUnfold[A, B, C](a: Stream[A], b: Stream[B], f: (Option[A], Option[B]) => C): Stream[C] =
    unfold((a, b)) {
      case (Cons(aH, aT), Cons(bH, bT)) => Some((f(Some(aH()), Some(bH())), (aT(), bT())))
      case (Empty, Cons(bH, bT)) => Some((f(None, Some(bH())), (Empty, bT())))
      case (Cons(aH, aT), Empty) => Some((f(Some(aH()), None), (aT(), Empty)))
      case _ => None
    }
}