package fpinscala.errorhandling

import scala.{Option => _, Either => _, Left => _, Right => _, _} // hide std library `Option` and `Either`, since we are writing our own in this chapter

sealed trait Either[+E,+A] {

  /* Exercise 4.6
   * Implement versions of map, flatMap, orElse, and map2 on Either that operate on the
   * Right value.
   */
 def map[B](f: A => B): Either[E, B] = this match {
   case l: Left[E] => Left(l.get)
   case r: Right[B] => Right(f(r.get))
 }

  /* Exercise 4.6
   * Implement versions of map, flatMap, orElse, and map2 on Either that operate on the
   * Right value.
   */
 def flatMap[EE >: E, B](f: A => Either[EE, B]): Either[EE, B] = this match {
   case l: Left[E] => Left(l.get)
   case r: Right[B] => f(r.get)
 }

  /* Exercise 4.6
   * Implement versions of map, flatMap, orElse, and map2 on Either that operate on the
   * Right value.
   */
 def orElse[EE >: E, B >: A](b: => Either[EE, B]): Either[EE, B] = this match {
   case l: Left[E] => b
   case _ => this
 }

  /* Exercise 4.6
   * Implement versions of map, flatMap, orElse, and map2 on Either that operate on the
   * Right value.
   */
 def map2[EE >: E, B, C](b: Either[EE, B])(f: (A, B) => C): Either[EE, C] = this match {
   case l: Left[E] => Left(l.get)
   case r: Right[A] => b match {
     case l2: Left[EE] => Left(l2.get)
     case r2: Right[B] => Right(f(r.get, r2.get))
   }
 }
}
case class Left[+E](get: E) extends Either[E,Nothing]
case class Right[+A](get: A) extends Either[Nothing,A]

object Either {
  /* Exercise 4.7
   * Implement traverse for Either. These should return the first error that’s encountered, if there is one.
   * Try for an implementation that only looks at the list once.
   */
  def traverse[E,A,B](es: List[A])(f: A => Either[E, B]): Either[E, List[B]] = {
    def go(l: List[A], acc: List[B]): Either[E, List[B]] = {
      l match {
        case Nil => Right(acc)
        case x :: xs => f(x) match {
          case l: Left[E] => Left(l.get)
          case r: Right[A] => go(xs, acc :+ r.get)
        }
      }
    }

    go(es, Nil)
  }

  /* Exercise 4.7
   * Implement sequence for Either. These should return the first error that’s encountered, if there is one.
   *
   * If the original list contains Left even once, the result of the function should be Left;
   * otherwise the result should be Some with a list of all Rights.
   */
  def sequence[E,A](es: List[Either[E,A]]): Either[E,List[A]] = traverse(es) {e => e}

  def mean(xs: IndexedSeq[Double]): Either[String, Double] = 
    if (xs.isEmpty) 
      Left("mean of empty list!")
    else 
      Right(xs.sum / xs.length)

  def safeDiv(x: Int, y: Int): Either[Exception, Int] = 
    try Right(x / y)
    catch { case e: Exception => Left(e) }

  def Try[A](a: => A): Either[Exception, A] =
    try Right(a)
    catch { case e: Exception => Left(e) }

}