package fpinscala.errorhandling


import scala.{Option => _, Some => _, Either => _, _} // hide std library `Option`, `Some` and `Either`, since we are writing our own in this chapter

// TODO implement all besides map and flatMap w/out pattern matching
// TODO is there a better way to deal with asInstanceOf[Some[A]] cases?
sealed trait Option[+A] {

  /* Exercise 4.1
   * - It’s fine to use pattern matching, though you should be able to implement all the functions
   * besides map and getOrElse without resorting to pattern matching.
   *
   * - For map and flatMap, the type signature should be enough to determine the implementation.
   * */
  def map[B](f: A => B): Option[B] = this match {
    case Some(x) => Some(f(x))
    case _ => None
  }

  /* Exercise 4.1
   * - It’s fine to use pattern matching, though you should be able to implement all the functions
   * besides map and getOrElse without resorting to pattern matching.
   *
   * - getOrElse returns the result inside the Some case of the Option, or if the Option is None,
   * returns the given default value.
   * */
  def getOrElse[B >: A](default: => B): B = this match {
    case Some(x) => x
    case _ => default
  }

  /* Exercise 4.1
   * It’s fine to use pattern matching, though you should be able to implement all the functions
   * besides map and getOrElse without resorting to pattern matching.
   * */
  def flatMap[B](f: A => Option[B]): Option[B] = this.map(f).getOrElse(None)

  /* Exercise 4.1
   * - It’s fine to use pattern matching, though you should be able to implement all the functions
   * besides map and getOrElse without resorting to pattern matching.
   *
   * - orElse returns the first Option if it’s defined; otherwise, it returns the second Option.
   * */
  def orElse[B >: A](ob: => Option[B]): Option[B] = if (this == None) ob else this

  /* Exercise 4.1
   * It’s fine to use pattern matching, though you should be able to implement all the functions
   * besides map and getOrElse without resorting to pattern matching.
   * */
  def filter(f: A => Boolean): Option[A] = this.flatMap(a => if (f(a)) this else None)
}

case class Some[+A](get: A) extends Option[A]
case object None extends Option[Nothing]

object Option {
  def failingFn(i: Int): Int = {
    val y: Int = throw new Exception("fail!") // `val y: Int = ...` declares `y` as having type `Int`, and sets it equal to the right hand side of the `=`.
    try {
      val x = 42 + 5
      x + y
    }
    catch { case e: Exception => 43 } // A `catch` block is just a pattern matching block like the ones we've seen. `case e: Exception` is a pattern that matches any `Exception`, and it binds this value to the identifier `e`. The match returns the value 43.
  }

  def failingFn2(i: Int): Int = {
    try {
      val x = 42 + 5
      x + ((throw new Exception("fail!")): Int) // A thrown Exception can be given any type; here we're annotating it with the type `Int`
    }
    catch { case e: Exception => 43 }
  }

  def mean(xs: Seq[Double]): Option[Double] =
    if (xs.isEmpty) None
    else Some(xs.sum / xs.length)

  /* Exercise 4.2
   * Implement the variance function in terms of flatMap.
   * If the mean of a sequence is m, the variance is the mean of math.pow(x - m, 2) for each element
   * x in the sequence. See the definition of variance on Wikipedia (http://mng.bz/0Qsr).
   */
  def variance(xs: Seq[Double]): Option[Double] =
    mean(xs).flatMap((m) => Some(xs.map((x) => Math.pow(x - m, 2)))).flatMap(mean(_))

  /* Exercise 4.3
   * Write a generic function map2 that combines two Option values using a binary function.
   * If either Option value is None, then the return value is too.
   */
  def map2[A,B,C](a: Option[A], b: Option[B])(f: (A, B) => C): Option[C] =
    a.map((valOfA) => b.map((valOfB) => f(valOfA, valOfB))).getOrElse(None)

  /* Exercise 4.4
   * Write a function sequence that combines a list of Options into one Option containing a list of
   * all the Some values in the original list. If the original list contains None even once, the
   * result of the function should be None; otherwise the result should be Some with a list of all
   * the values.
   */
  def sequence[A](a: List[Option[A]]): Option[List[A]] = {
    def go(cur: Option[A], source: List[Option[A]], acc: List[A]): Option[List[A]] = cur match {
      case None => None
      case Some(x) => {
        if (source.isEmpty)
          Some(acc :+ x)
        else
          go(source.head, source.tail, acc :+ x)
      }
    }

    go(a.head, a.tail, Nil)
  }

  /* Exercise 4.5
   * Implement traverse. It’s straightforward to do using map and sequence, but try for a more efficient
   * implementation that only looks at the list once. In fact, implement sequence in terms of traverse.
   */
  def traverse[A, B](a: List[A])(f: A => Option[B]): Option[List[B]] = {
    Some(a.foldLeft(List[B]())((acc, srcEl) => {
      f(srcEl) match {
        case None => acc
        case s: Some[B] => acc :+ s.get
      }
    }))
  }

  /* Exercise 4.5
   * Implement sequence in terms of traverse.
   */
  def sequenceInTermsOfTraverse[A](a: List[Option[A]]): Option[List[A]] = {
    traverse[Option[A],A](a)((opt) => opt) match {
      case None => None
      case s: Some[List[A]] => if (s.get.size == a.size) s else None
    }
  }
}