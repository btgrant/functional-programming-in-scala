package fpinscala.datastructures

sealed trait Tree[+A]
case class Leaf[A](value: A) extends Tree[A]
case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

object Tree {

  /* Exercise 3.25
   * Write a function size that counts the number of nodes (leaves and branches) in a tree.
   */
  def size[A](t: Tree[A]): Int = t match {
    case b: Branch[A] => 1 + size(b.left) + size(b.right)
    case _ => 1
  }

  /* Exercise 3.29 */
  def sizeByFold[A](t: Tree[A]): Int = {
    fold[A,Int](t)(_ => 1)(_ + _ + 1)
  }

  /* Exercise 3.26
   * Write a function maximum that returns the maximum element in a Tree[Int].
   * Note: In Scala, you can use x.max(y) or x max y to compute the maximum of two integers x and y.
   */
  def maximum(t: Tree[Int]): Int = t match {
      case l: Leaf[Int] => l.value
      case b: Branch[Int] => maximum(b.left).max(maximum(b.right))
    }

  /* Exercise 3.29 */
  def maximumByFold(t: Tree[Int]): Int = {
    fold[Int,Int](t)(x => x)((x, y) => x.max(y))
  }

  /* Exercise 3.27
   * Write a function depth that returns the maximum path length from the root of a tree to any leaf.
   */
  def depth[A](t: Tree[A]): Int = t match {
      case b: Branch[A] => (depth(b.right) + 1).max(depth(b.left) + 1)
      case _ => 1
    }

  /* Exercise 3.29 */
  def depthByFold[A](t: Tree[A]): Int = {
    fold[A,Int](t)(_ => 1)((x, y) => (x + 1).max(y + 1))
  }

  /* Exercise 3.28
   * Write a function map, analogous to the method of the same name on List, that modifies each
   * element in a tree with a given function.
   */
  def map[A,B](t: Tree[A], f: A => B): Tree[B] = t match {
    case l: Leaf[A] => Leaf(f(l.value))
    case b: Branch[A] => Branch(map(b.left, f), map(b.right, f))
  }

  /* Exercise 3.29 */
  def mapByFold[A,B](t: Tree[A], f: A => B): Tree[B] = {
    fold[A,Tree[B]](t)((v: A) => Leaf(f(v)))((x, y) => Branch(x, y))
  }

  /* Exercise 3.29
   * Generalize size, maximum, depth, and map, writing a new function fold that abstracts over their
   * similarities. Reimplement them in terms of this more general function.
   * Can you draw an analogy between this fold function and the left and right folds for List?
   */
  def fold[A,B](t: Tree[A])(f: (A) => B)(g: (B, B) => B): B = t match {
    case l: Leaf[A] => f(l.value)
    case b: Branch[A] => g(fold(b.left)(f)(g), fold(b.right)(f)(g))
  }
}