package fpinscala.datastructures

import scala.annotation.tailrec

sealed trait List[+A] // `List` data type, parameterized on a type, `A`
case object Nil extends List[Nothing] // A `List` data constructor representing the empty list
/* Another data constructor, representing nonempty lists. Note that `tail` is another `List[A]`,
which may be `Nil` or another `Cons`.
 */
case class Cons[+A](head: A, tail: List[A]) extends List[A]

object List { // `List` companion object. Contains functions for creating and working with lists.
  def sum(ints: List[Int]): Int = ints match { // A function that uses pattern matching to add up a list of integers
    case Nil => 0 // The sum of the empty list is 0.
    case Cons(x,xs) => x + sum(xs) // The sum of a list starting with `x` is `x` plus the sum of the rest of the list.
  }

  /* Exercise 3.11
   * Write sum, product, and a function to compute the length of a list using foldLeft.
   */
  def sumByFoldLeft(ns: List[Int]): Int = foldLeft(ns, 0)(_ + _)
    /*foldRight(ns, 0)((x,y) => x + y)*/

  def product(ds: List[Double]): Double = ds match {
    case Nil => 1.0
    case Cons(0.0, _) => 0.0
    case Cons(x,xs) => x * product(xs)
  }

  /* Exercise 3.11
   * Write sum, product, and a function to compute the length of a list using foldLeft.
   */
  def productByFoldLeft(ns: List[Double]): Double = foldLeft(ns, 1.0)(_ * _)
  /*foldRight(ns, 1.0)(_ * _)*/ // `_ * _` is more concise notation for `(x,y) => x * y`; see sidebar

  def apply[A](as: A*): List[A] = // Variadic function syntax
    if (as.isEmpty) Nil
    else Cons(as.head, apply(as.tail: _*))

  val x = List(1,2,3,4,5) match {
    case Cons(x, Cons(2, Cons(4, _))) => x
    case Nil => 42
    case Cons(x, Cons(y, Cons(3, Cons(4, _)))) => x + y
    case Cons(h, t) => h + sum(t)
    case _ => 101
  }

  def append[A](a1: List[A], a2: List[A]): List[A] = a1 match {
      case Nil => a2
      case Cons(h,t) => Cons(h, append(t, a2))
    }

  /* Exercise 3.14
   * Implement append in terms of either foldLeft or foldRight.
   */
  def appendInTermsOfFold[A](a1: List[A], a2: List[A]): List[A] = foldRight(a1, a2)((src, acc) => Cons(src, acc))

  /* TODO Exercise 3.15 HARD
   * Write a function that concatenates a list of lists into a single list.
   * Its runtime should be linear in the total length of all lists.
   * Try to use functions we have already defined.
   */

  def foldRight[A,B](as: List[A], z: B)(f: (A, B) => B): B = // Utility functions
    as match {
      case Nil => z
      case Cons(x, xs) => f(x, foldRight(xs, z)(f))
    }

  def main(args: Array[String]): Unit = {
    println("product of [2, 5, 0, 8, 9, 4, 2] is :  " + List.product(List(2, 5, 0, 8, 9, 4, 2)))
    println("product of [2, 5, 0, 8, 9, 4, 2] is :  " + List.productByFoldLeft(List(2, 5, 0, 8, 9, 4, 2)))

    println(s"result is ${foldRight(List(1,2,3), Nil:List[Int])(Cons(_,_))}")
  }

  /* Exercise 3.2
   * Implement the function tail for removing the first element of a List.
   */
  def tail[A](l: List[A]): List[A] = {
    l match {
      case Nil => Nil
      case Cons(_, xs) => xs
    }
  }

  /* Exercise 3.4
   * Generalize tail to the function drop, which removes the first n elements from a list.
   */
  def tailByDrop[A](l: List[A]): List[A] = drop(l, 1)

  /* Exercise 3.3
   * Implement the function setHead for replacing the first element of a List with a different value.
   */
  def setHead[A](l: List[A], h: A): List[A] =
    l match {
      case Nil => List(h)
      case Cons(_, xs) => Cons(h, xs)
    }

  def drop[A](l: List[A], n: Int): List[A] = {
    @tailrec
    def go(list: List[A], acc: Int): List[A] = {
      if (acc == n) {
        list
      } else list match {
        case Nil => Nil
        case Cons(_, xs) => go(xs, acc + 1)
      }
    }

    go(l, 0)
  }

  /* Exercise 3.5
   * Implement dropWhile, which removes elements from the List prefix as long as they match a predicate.
   */
  def dropWhile[A](l: List[A], f: A => Boolean): List[A] = {
    def go(l: List[A]): List[A] =
      l match {
        case Nil => Nil
        case Cons(x, xs) => if (f(x)) {
          go(xs)
        } else {
          Cons(x, go(xs))
        }
      }

    go(l)
  }

  /* Exercise 3.6
   * Implement a function, init, that returns a List consisting of all but the last element of a List.
   * So, given List(1,2,3,4), init will return List(1,2,3).
   */
  def init[A](l: List[A]): List[A] = {
    def go(l: List[A]): List[A] = l match {
      case Nil => Nil
      case Cons(_, Nil) => Nil
      case Cons(x, xs) => Cons(x, go(xs))
    }

    go(l)
  }

  /* Exercise 3.9
   * Computer the length of a list using foldRight.
   */
  def length[A](l: List[A]): Int = foldRight(l, 0)((_, count) => count + 1)

  /* Exercise 3.11
   * Write sum, product, and a function to compute the length of a list using foldLeft.
   */
  def lengthWithFoldLeft[A](l: List[A]): Int = foldLeft(l, 0)((count, _) => count + 1)

  /* Exercise 3.10
   * Our implementation of foldRight is not tail-recursive and will result in a StackOverflowError
   * for large lists (we say it’s not stack-safe). Convince yourself that this is the case,
   * and then write another general list-recursion function, foldLeft, that is tail-recursive,
   * using the techniques we discussed in the previous chapter.
   */
  @tailrec
  def foldLeft[A,B](l: List[A], z: B)(f: (B, A) => B): B =
    l match {
      case Nil => z
      case Cons(x, xs) => foldLeft(xs, f(z, x))(f)
    }

  /* Exercise 3.12
   * Write a function that returns the reverse of a list (given List(1,2,3) it returns List(3,2,1)).
   * See if you can write it using a fold.
   */
  def reverse[A](l: List[A]): List[A] = foldLeft(l, List[A]())((acc, src) => append(List(src), acc))
    /*def go(source: List[A]): List[A] =
      source match {
        case Nil => source
        case Cons(x, xs) => append(go(xs), List(x))
      }

    go(l)*/

  /* TODO Exercise 3.13 (optional)
   * HARD:  Can you write foldLeft in terms of foldRight? How about the other way around?
   * Implementing foldRight via foldLeft is useful because it lets us implement foldRight tail-recursively,
   * which means it works even for large lists without overflow- ing the stack.
   */
  def foldLeftInTermsOfFoldRightOrVsv = ???

  /* Exercise 3.16
   * Write a function that transforms a list of integers by adding 1 to each element.
   */
  def addOne(l: List[Int]): List[Int] = foldRight(l, List[Int]())((num, acc) => Cons(num + 1, acc))

  /* Exercise 3.17
   * Write a function that turns each value in a List[Double] into a String.
   * You can use the expression d.toString to convert some d: Double to a String.
   */
  def doublesToStrings(l: List[Double]): List[String] = foldRight(l, List[String]())((num, strings) => Cons(num.toString, strings))

  /* Exercise 3.18
   * Write a function map that generalizes modifying each element in a list while
   * maintaining the structure of the list.
   */
  def map[A,B](l: List[A])(f: A => B): List[B] = foldRight(l, List[B]())((a, acc) => Cons(f(a), acc))

  /* Exercise 3.19
   * Write a function filter that removes elements from a list unless they satisfy a given predicate.
   * Use it to remove all odd numbers from a List[Int].
   */
  def filter[A](as: List[A])(f: A => Boolean): List[A] = {
    foldRight(as, List[A]()){(srcEl, acc) => if (f(srcEl)) Cons(srcEl, acc) else acc }
  }

  /* Exercise 3.21
   * Use flatMap to implement filter
   */
  def filterByFlatMap[A](as: List[A])(f: A => Boolean): List[A] =
    flatMap(as)((a) => if (f(a)) List(a) else Nil)

  /* Exercise 3.20
   * Write a function flatMap that works like map except that the function given will return a list
   * instead of a single result, and that list should be inserted into the final resulting list.
   */
  def flatMap[A,B](as: List[A])(f: A => List[B]): List[B] = {
    foldRight(as, List[B]()) {(srcEl, acc) => append(f(srcEl), acc)}
  }

  /* Exercise 3.22
   * Write a function that accepts two lists and constructs a new list by adding corresponding elements.
   * For example, List(1,2,3) and List(4,5,6) become List(5,7,9).
   */
  def addPairwise(a: List[Int], b: List[Int]): List[Int] = (a,b) match {
    case (Nil, _) => Nil
    case (_, Nil) => Nil
    case (Cons(aHead, aTail), Cons(bHead, bTail)) => Cons(aHead + bHead, addPairwise(aTail, bTail))
  }

  /* Exercise 3.23
   * Generalize addPairwise so that it’s not specific to integers or addition.
   * Name your generalized function zipWith.
   */
  def zipWith[A,B,C](a: List[A], b: List[B], f: (A, B) => C): List[C] = (a, b) match {
    case (Nil, _) => Nil
    case (_, Nil) => Nil
    case (Cons(ah, at), Cons(bh, bt)) =>  Cons(f(ah, bh), zipWith(at, bt, f))
  }

  /* TODO Exercise 3.24 HARD
   * Implement hasSubsequence for checking whether a List contains another List as a subsequence.
   * For instance, List(1,2,3,4) would have List(1,2), List(2,3), and List(4) as subsequences, among others.
   * You may have some difficulty finding a concise purely functional implementation that is also efficient.
   * That’s okay. Implement the function however comes most naturally.
   * We’ll return to this implementation in chapter 5 and hopefully improve on it.
   *
   * Note: Any two values x and y can be compared for equality in Scala using the expression x == y.
   */
  def hasSubsequence[A](sup: List[A], sub: List[A]): Boolean = (sup, sub) match {
    case (_, Nil) => false
    case (Nil, _) => false
    case _ => {
      def go(sup: List[A], sub: List[A], acc: List[A], accB: Boolean): Boolean = {
        (sup, sub) match {
          case (_, Nil) => accB
          case (Nil, _) => accB
          case (Cons(supH, supT), Cons(subH, subT)) => true
        }
      }

      go(sup, sub, Nil, false)
    }
  }

}
