import sbt.Tests

val commonSettings = Seq(
  scalaVersion := "2.12.1"
)

lazy val root = (project in file("."))
  .aggregate(exercises, answers)
  .settings(commonSettings)
  .settings(
    name := "FP in Scala book"
  )

lazy val exercises = (project in file("exercises"))
  .settings(commonSettings)
  .settings(
    name := "exercises"
  ).settings(
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"
  ).settings(
      testOptions in Test += Tests.Argument(TestFrameworks.ScalaTest, "-oI") // via https://stackoverflow.com/questions/21806312/how-to-configure-sbt-test-scalatest-to-only-show-failures
  ).settings(jacoco.settings)

lazy val answers = (project in file("answers"))
  .settings(commonSettings)
  .settings(
    name := "answers"
  )
